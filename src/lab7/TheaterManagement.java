package lab7;

import java.util.ArrayList;

public class TheaterManagement {

	private ArrayList<ArrayList<Seat>> showtime;
	private ArrayList<Seat> array1;
	private ArrayList<Seat> array2;
	private ArrayList<Seat> array3;

	public TheaterManagement() {
		showtime = new ArrayList<ArrayList<Seat>>();
		array1 = new ArrayList<Seat>();
		array2 = new ArrayList<Seat>();
		array3 = new ArrayList<Seat>();
		showtime.add(array1);
		showtime.add(array2);
		showtime.add(array3);
	}

	public int buyTickets(int time, char row, int col) {
		Seat seat = new Seat(time, row, col);
		if (!showtime.get(time - 1).contains(seat)) {
			showtime.get(time - 1).add(seat);
			
			return (int) DataSeatPrice.ticketPrices[row - 65][col - 1];
		} else {
			return 0;
		}
	}

	public ArrayList<String> checkSeatByPrice(int price) {
		ArrayList<String> str = new ArrayList<String>();
		for (int i = 0; i < DataSeatPrice.ticketPrices.length; i++) {
			for (int j = 0; j < DataSeatPrice.ticketPrices[i].length; j++) {
				if (price == DataSeatPrice.ticketPrices[i][j]) {
					for (int round = 0; round < showtime.size(); round++) {
						Seat seat = new Seat(round+1, (char)(i + 65), j+1);	//���ҧ seat �������º�Ѻ�Ѻ��÷Ѵ��ҧ
						if (!showtime.get(round).contains(seat)) {		//��º�Ѻ array �ͧ seat ���ͧ�������� 
							str.add((char) (i + 65) + Integer.toString(j + 1)
									+ "round" + (round + 1));
						}
					}
				}
			}
		}
		return str;

	}
	
	public ArrayList<String> checkSeatBySeat(char row, int col){
		ArrayList<String> str = new ArrayList<String>();
		for (int round = 0; round < showtime.size(); round++) {
			Seat seat = new Seat(round+1, row, col);	
			if (!showtime.get(round).contains(seat)) {			
				str.add(row +  Integer.toString(col)+ "round" + (round + 1));
			}
		}
		return str;
	}

}
