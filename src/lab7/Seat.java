package lab7;


public class Seat {
	int time, col;
	char  row;
	public Seat(int time,char row, int col){
		this.time = time;
		this.col = col;
		this.row = row;
	}
	
	public boolean equals(Object other){
		if(other == null)return false;
		if(!(other instanceof Seat)) return false;
		Seat otherSeat = ((Seat) other);
		if(otherSeat.time == this.time
				&&otherSeat.row == this.row
				&&otherSeat.col == this.col){
			return true;			
		}else return false;
	}
}
